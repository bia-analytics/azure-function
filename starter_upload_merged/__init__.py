from datetime import datetime
import logging

from azure.core.exceptions import ResourceNotFoundError
from azure.data.tables import TableClient
import azure.functions as func
import pandas as pd
 
from env_vars_lib import get_env_vars
from extract_load_lib import load_to_blob_storage


def main(mytimer: func.TimerRequest) -> None:
    
    env_vars = get_env_vars()

    table_client = TableClient.from_connection_string(
        env_vars.storage_conn_string, 
        env_vars.realtimesales_storage_table
    )
    try:
        data = list(table_client.list_entities())
    except ResourceNotFoundError:
        return
    
    if not data:
        logging.info(f"There isn't any sales data in Storage Table{env_vars.realtimesales_storage_table}")
        return

    df = pd.DataFrame(data)

    blob_name = f'real_time_sales_{datetime.today().isoformat()}.csv'
    load_to_blob_storage(
        df,
        'csv',
        env_vars.storage_conn_string,
        env_vars.gold_container,
        env_vars.stream_folder,
        blob_name
    )