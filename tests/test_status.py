from datetime import datetime
import logging
import time

from extract_load_lib import ETLStatus


def test_status_class():
    start_time = datetime.now()

    status = ETLStatus('000305', 'e-commerce', '1.1.1.1')
    status.add_status_msg('something is wrong')
    status.add_start_time(start_time)
    try:
        1/0
    except Exception as e:
        status.add_exception(e)

    time.sleep(2)
    status.add_end_time(datetime.now())
    logging.debug(status.to_dict())

    assert isinstance(status, ETLStatus)