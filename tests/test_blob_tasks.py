import logging

from azure.storage.blob import BlobServiceClient
import pytest

from starter_clean import move_blob


@pytest.fixture
def source_path():
    return 'test/source/test.txt'

@pytest.fixture
def target_path():
    return 'test/target/test.txt'

def test_move_blob(storage_conn_string, bronze_container_name,
     source_path, target_path):
    
    container = BlobServiceClient.from_connection_string(storage_conn_string) \
        .get_container_client(bronze_container_name)
    blob_source = container.get_blob_client(source_path)
    blob_source.upload_blob('content string', overwrite=True)

    response = move_blob(container, source_path, target_path)
    logging.debug(response)
    
    assert True

    blob_target = container.get_blob_client(target_path)
    blob_target.delete_blob()