from configparser import ConfigParser
import logging

from extract_load_lib import get_tables_to_query


def test_get_tables_to_query():
    tables_to_query = get_tables_to_query()
    
    logging.debug(f'config sections are: {tables_to_query.sections()}')
    logging.debug(f'table_name example: {tables_to_query["LOJA_VENDA"]["table_name"]}')
    logging.debug(f'query example: {tables_to_query["LOJA_VENDA"]["query"]}')

    assert isinstance(tables_to_query, ConfigParser)