import logging
import os

import pytest

from get_databases import main


@pytest.fixture
def database_list_file_path():
    return os.environ.get('DATABASE_LIST_FILE_PATH')

@pytest.fixture
def database_list_file_name():
    return os.environ.get('DATABASE_LIST_FILE_NAME')

def test_get_databases(
    storage_conn_string,
    bronze_container_name,
    database_list_file_path,
    database_list_file_name
):
    filedata = {
        'conn_string': storage_conn_string,
        'container': bronze_container_name,
        'database_list_file_path': database_list_file_path,
        'database_list_file_name': database_list_file_name
    }
    db_list = main(filedata)
    logging.debug(f'first item of the list: \n {db_list[0]}')
    assert isinstance(db_list, list)