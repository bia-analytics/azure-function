from datetime import datetime
import logging
from pathlib import Path

import pandas as pd

from extract_load_lib import SalesTableComposer


def test_transform_sales():
    sales_date = datetime(2021, 12, 13)

    prefix_path = Path(__file__).parent / 'dummy_data'

    loja_venda_pgto_path = prefix_path / 'parquets/loja_venda_pgto.parquet'
    loja_venda_path = prefix_path / 'parquets/loja_venda.parquet'
    vendedores_path = prefix_path / 'parquets/loja_vendedores.parquet'
    filiais_path = prefix_path / 'parquets/filiais.parquet'
    goals_path = prefix_path / 'csv/202112.csv'

    df_loja_venda_pgto = pd.read_parquet(loja_venda_pgto_path)
    df_loja_venda = pd.read_parquet(loja_venda_path)
    df_vendedores = pd.read_parquet(vendedores_path)
    df_filiais = pd.read_parquet(filiais_path)
    df_goals = pd.read_csv(goals_path, sep=';', encoding='utf-8')



    df = SalesTableComposer().compose(
        sales_date,
        df_loja_venda_pgto,
        df_loja_venda,
        df_vendedores,
        df_filiais,
        df_goals
    )

    logging.debug('\n' + f'{df.dtypes}')
    logging.debug(f'number of rows: {len(df)}')

    assert df.empty == False