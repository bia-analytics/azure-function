import io
import logging
from pathlib import Path

from azure.storage.blob import BlobServiceClient
import pandas as pd
import pytest

from extract_load_lib import load_to_blob_storage
from extract_load_lib.load import _convert_df_to_parquet_bytes


@pytest.fixture(scope='function')
def load_blob_path():
    return 'test/load/'

@pytest.fixture
def load_blob_name(scope='function'):
    return 'test'

@pytest.fixture(scope='function')
def dummy_df():
    dummy_data_path = Path(__file__).parent / 'dummy_data/dummy_data.json'
    return pd.read_json(dummy_data_path, orient="records")

def test_convert_df_to_parquet_bytes(dummy_df):
    parquet_file = _convert_df_to_parquet_bytes(dummy_df)
    parquet_content_df = pd.read_parquet(parquet_file)
    logging.debug(parquet_content_df)
    
    assert parquet_content_df.empty == False

def test_load_data(
    storage_conn_string,
    bronze_container_name, 
    load_blob_path, 
    load_blob_name, 
    dummy_df):
    
    load_result = load_to_blob_storage(
        dataframe=dummy_df,
        format='parquet',
        conn=storage_conn_string,
        container=bronze_container_name,
        blob_path=load_blob_path,
        blob_name=f'{load_blob_name}.parquet'
    )

    logging.debug(f'load result : {load_result}')

    blob_client = BlobServiceClient.from_connection_string(storage_conn_string) \
        .get_blob_client(
            bronze_container_name, 
            f'{load_blob_path}{load_blob_name}.parquet')
        
    stream_downloader = blob_client.download_blob()
    stream = io.BytesIO()
    stream_downloader.readinto(stream)

    logging.debug(pd.read_parquet(stream, engine='pyarrow'))

    assert True

    blob_client.delete_blob()