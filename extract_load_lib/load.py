"""Library for loading data to blob storage."""

import io
import logging

from azure.storage.blob import BlobServiceClient


def _convert_df_to_parquet_bytes(dataframe):
    """Convert a Pandas DataFrame to parquet (bytes)."""

    logging.debug('convert dataframe to parquet start')

    parquet_file = io.BytesIO()
    dataframe.to_parquet(parquet_file, engine="pyarrow")
    parquet_file.seek(0)

    logging.debug('convert dataframe to parquet end')

    return parquet_file

def _convert_df_to_csv_string(dataframe):
    """Convert a Pandas DataFrame to csv (bytes)."""

    logging.debug('convert dataframe to csv start')

    csv_string = dataframe.to_csv(
        sep=',',
        index=False,
        encoding='utf-8'
    )

    logging.debug('convert dataframe to csv end')

    return csv_string

def load_to_blob_storage(dataframe, format, conn, container, blob_path, blob_name):
    """Load Pandas DataFrame to a blob container in parquet format.
    
    Parameters
    ----------
    dataframe: DataFrame
        pandas DataFrame for uploading.
    format: str
        formats can be csv or parquet,
    conn: str
        blob storage connection string obtained in azure portal .
    container: str
        name of the target container.
    blob_path: str
        path where parquet file will be stored.
    blob_name: str
        name of the blob (e.g. dataframe.parquet).
    
    Returns
    -------
    dict
        Upload result from blob storage client.
    """

    logging.debug('load data start')

    format_method = {
        'csv': _convert_df_to_csv_string,
        'parquet': _convert_df_to_parquet_bytes
    }

    data = format_method[format](dataframe)

    blob_service_client = BlobServiceClient \
        .from_connection_string(conn)
    blob_client = blob_service_client.get_blob_client(
        container=container,
        blob=blob_path + blob_name
    )

    load_result = blob_client.upload_blob(data=data, overwrite=True)

    logging.debug('load data end')

    return load_result