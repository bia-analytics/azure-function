from dataclasses import dataclass
from datetime import datetime

@dataclass
class ETLStatus:
    """Class for instantiating and returning ETL process status"""

    store_code: str = None
    store_name: str = None
    store_host: str = None
    start_time: datetime = None
    end_time: datetime = None
    status_msg: str = None
    
    def to_dict(self):
        """Return a dict representing class parameters."""
        
        if not self.end_time:
            self.end_time = datetime.now()
        self.elapsed_time = self.end_time.timestamp() - self.start_time.timestamp()
        self.start_time = str(self.start_time)
        self.end_time = str(self.end_time)

        return self.__dict__

    def add_exception(self, exception: Exception):
        """Add exception and exception class parameters to the class."""

        self.exception_msg = f'{exception}'.replace('\n', '').replace(';', '')
        self.exception_class = str(type(exception))