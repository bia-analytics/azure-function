import calendar
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
from pandas.core.api import DataFrame


REGIONS = {
    'AC': 'Norte',
    'AM': 'Norte',
    'RR': 'Norte',
    'AP': 'Norte', 
    'PA': 'Norte',
    'RO': 'Norte',
    'TO': 'Norte',
    'MT': 'Centro Oeste',
    'DF': 'Centro Oeste',
    'GO': 'Centro Oeste',
    'MS': 'Centro Oeste',
    'MA': 'Nordeste',
    'CE': 'Nordeste',
    'PI': 'Nordeste',
    'BA': 'Nordeste',
    'RN': 'Nordeste',
    'PB': 'Nordeste',
    'PE': 'Nordeste',
    'AL': 'Nordeste',
    'SE': 'Nordeste',
    'MG': 'Sudeste',
    'SP': 'Sudeste',
    'ES': 'Sudeste',
    'RJ': 'Sudeste',
    'PR': 'Sul',
    'SC': 'Sul',
    'RS': 'Sul'
}

class SalesTableComposer:
    """Class for wrapping all sales table transformations"""

    @staticmethod
    def _strip_dataframe(df: DataFrame):
        """Strip all string columns"""

        df_obj = df.select_dtypes(['object'])
        df[df_obj.columns] = df_obj.apply(lambda x: x.strip() if isinstance(x, str) else x)

        return df

    @staticmethod
    def _filter_date(
        column_to_filter: str, 
        sales_date: datetime,
        df: DataFrame) -> DataFrame:
        """Filter dataframe by the given datetime and column to filter"""

        return df[df[column_to_filter] == sales_date.strftime('%Y-%m-%d')]

    @staticmethod
    def _return_basic_transformations(df: DataFrame, df_last_year: DataFrame) -> DataFrame:
        """Return dataframe with basic aggregations and columns"""

        goal = df['VALOR'].max()

        df = df.groupby([
            'COD_FILIAL_LOJA_VENDA_PGTO', 
            'FILIAL', 
            'TIPO_FILIAL',
            'CIDADE',
            'UF',
            'INDICA_FRANQUIA',
            'OPERACAO_VENDA',
            'CODIGO_DESCONTO',
            'SUPERVISOR',
            'SUPERVISOR_REGIONAL'], 
            as_index=False,
            dropna=False
        ).agg(
            Valor_Venda_Faturada_Bruto=('VALOR_VENDA_BRUTA', sum),
            Valor_Venda_Desconto_Venda=('DESCONTO', lambda x: (x > 0).sum()),
            Valor_Venda_Desconto_Pagamento=('DESCONTO_PGTO', lambda x: (x > 0).sum()),
            # Valor_Venda_Acrescimo_Venda=('DESCONTO', lambda x: abs((x < 0).sum())),
            # Valor_Venda_Acrescimo_Pagamento=('DESCONTO_PGTO', lambda x: abs((x < 0).sum())),
            # Valor_Venda_Trocas=('VALOR_TROCA', sum),
            # Valor_Venda_Devolucoes=('TOTAL_VENDA', lambda x: abs((x < 0).sum())),
            Valor_Venda_Cancelado=('VALOR_CANCELADO', sum),
            Valor_Venda_Faturada_Liquido=('TOTAL_VENDA', sum),
            Quantidade_Venda_Tickets_Cancelado=('MOTIVO_CANCELAMENTO',
                lambda x: x.notnull().sum()),
            Quantidade_Venda_Tickets_Total=('TICKET', 'count'),
            Quantidade_Vendas_Pecas=('QTDE_TOTAL', sum),
            Quantidade_Venda_Itens_Troca=('QTDE_TROCA_TOTAL', sum)
            # Valor_Venda_Meta_Faturada_Liquido=('VALOR', max)
        )
        
        df['Valor_Venda_Meta_Faturada_Liquido'] = goal
        # df['Valor_Venda_Faturada_Liquido_Total_Geral'] \
        #     = df['Valor_Venda_Faturada_Liquido'].sum()
        df.rename(columns={'INDICA_FRANQUIA': 'CANAL_FILIAL'}, inplace=True) 
        df['CANAL_FILIAL'] = df['CANAL_FILIAL'] \
            .apply(lambda x: 'FRANQUIA' if x == True else 'PROPRIA')
        df['Valor_Venda_Faturada_Liquido_Ano_Anterior'] = \
            df_last_year['TOTAL_VENDA'].sum()
        df['REGIAO'] = df['UF'].map(REGIONS)

        return df

    @staticmethod
    def _return_aggregation_sums(df: DataFrame) -> DataFrame:
        """Return dataframe with sum columns"""

        # df['Valor_Venda_Desconto'] = df['Valor_Venda_Desconto_Venda'] \
        #     + df['Valor_Venda_Desconto_Pagamento']
        # df['Valor_Venda_Acrescimo'] = df['Valor_Venda_Acrescimo_Venda'] \
        #     + df['Valor_Venda_Acrescimo_Pagamento']
        df['Quantidade_Venda_Tickets'] = df['Quantidade_Venda_Tickets_Total'] \
            - df['Quantidade_Venda_Tickets_Cancelado']
        df['Quantidade_Venda_Itens'] = df['Quantidade_Vendas_Pecas'] \
            - df['Quantidade_Venda_Itens_Troca']
        
        return df

    def compose(
        self,
        sales_date: datetime,
        df_loja_venda_pgto: DataFrame,
        df_loja_venda_pgto_lastyear: DataFrame,
        df_loja_venda: DataFrame,
        df_cadastro_cli_for: DataFrame,
        df_filiais: DataFrame,
        df_goals: DataFrame,
        df_supervisores: DataFrame
    ) -> DataFrame:
        """Apply transformations and return final dataframe"""
        
        df_supervisores.dropna(subset=['CÓDIGO'], inplace=True)
        df_supervisores['CÓDIGO'] = df_supervisores['CÓDIGO'] \
            .astype(pd.Int64Dtype()).apply(lambda x: f'{x:06}')
        df_supervisores = self._strip_dataframe(df_supervisores)
        df_supervisores.drop_duplicates(inplace=True)
        df_supervisores.rename(columns={
                'CÓDIGO': 'CODIGO_FILIAL_SUPERVISORES',
                'SUPERVISOR REGIONAL': 'SUPERVISOR_REGIONAL'
            }, 
            inplace=True
        )

        df_goals['dt_data'] = pd.to_datetime(df_goals['dt_data'], dayfirst=True)
        df_goals.rename(columns={
                'dt_data': 'DATA_GOAL',
                'nk_filial': 'CODIGO_FILIAL_GOAL',
                'vl_meta': 'VALOR'
            },
            inplace=True
        )
        
        df_loja_venda_pgto = self._strip_dataframe(df_loja_venda_pgto)
        df_loja_venda_pgto.rename(columns={
                'CODIGO_FILIAL': 'COD_FILIAL_LOJA_VENDA_PGTO',
                'DATA': 'DATA_LOJA_VENDA_PGTO'
            },
            inplace=True
        )
        df_loja_venda = self._strip_dataframe(df_loja_venda)
        df_loja_venda.rename(columns={
                'CODIGO_FILIAL_PGTO': 'COD_FILIAL_LOJA_VENDA'
            },
            inplace=True
        )
        df_cadastro_cli_for = self._strip_dataframe(df_cadastro_cli_for)
        df_filiais = self._strip_dataframe(df_filiais)
        df_filiais.rename(columns={
                'COD_FILIAL': 'COD_FILIAL_FILIAIS'
            },
            inplace=True
        )
    
        df = df_loja_venda_pgto.merge(
            df_loja_venda,
            how='inner',
            left_on=['COD_FILIAL_LOJA_VENDA_PGTO', 'TERMINAL', 'LANCAMENTO_CAIXA', 'DATA_LOJA_VENDA_PGTO'],
            right_on=['COD_FILIAL_LOJA_VENDA', 'TERMINAL_PGTO', 'LANCAMENTO_CAIXA', 'DATA_VENDA']
        )
        df = df.merge(
            df_filiais,
            how='inner',
            left_on='COD_FILIAL_LOJA_VENDA_PGTO',
            right_on='COD_FILIAL_FILIAIS'
        )
        df = df.merge(
            df_cadastro_cli_for,
            how='inner',
            left_on='COD_FILIAL_LOJA_VENDA_PGTO',
            right_on='CLIFOR'
        )
        df = df.merge(
            df_goals,
            how='left',
            left_on=['COD_FILIAL_LOJA_VENDA_PGTO', 'DATA_LOJA_VENDA_PGTO'],
            right_on=['CODIGO_FILIAL_GOAL', 'DATA_GOAL']
        )
        df = df.merge(
            df_supervisores,
            how='left',
            left_on='COD_FILIAL_LOJA_VENDA_PGTO',
            right_on='CODIGO_FILIAL_SUPERVISORES'
        )

        # today = datetime.today().date()
        # days_delta = 365 if not calendar.isleap(today.year) else 366
        # df_last_year = self._filter_date('DATA_LOJA_VENDA_PGTO', sales_date - timedelta(days=days_delta), df)

        df = self._filter_date('DATA_LOJA_VENDA_PGTO', sales_date, df)
        df = self._return_basic_transformations(df, df_loja_venda_pgto_lastyear)
        df = self._return_aggregation_sums(df)

        # df['Valor_Referencia_Meta_Vendas_Liquida_Grafico'] \
        #     = np.select(
        #         [
        #             (df['Valor_Venda_Faturada_Liquido'].sum() > df['Valor_Venda_Faturada_Liquido_Ano_Anterior'].max())
        #                 & (df['Valor_Venda_Faturada_Liquido'].sum() > df['Valor_Venda_Meta_Faturada_Liquido'].max()),
        #             (df['Valor_Venda_Faturada_Liquido_Ano_Anterior'].max() > df['Valor_Venda_Faturada_Liquido'].sum())
        #                 & (df['Valor_Venda_Faturada_Liquido_Ano_Anterior'].max() > df['Valor_Venda_Meta_Faturada_Liquido'].max()),
        #             (df['Valor_Venda_Meta_Faturada_Liquido'].max() > df['Valor_Venda_Faturada_Liquido'].sum())
        #                 & (df['Valor_Venda_Meta_Faturada_Liquido'].max() > df['Valor_Venda_Faturada_Liquido_Ano_Anterior'].max())
        #         ],
        #         [
        #             df['Valor_Venda_Faturada_Liquido'].sum() * 1.30,
        #             df['Valor_Venda_Faturada_Liquido_Ano_Anterior'].max() * 1.30,
        #             df['Valor_Venda_Meta_Faturada_Liquido'].max() * 1.30
        #         ]
        #     )
        
        df = df.rename(columns={'COD_FILIAL_LOJA_VENDA_PGTO': 'CODIGO_FILIAL'})

        return df