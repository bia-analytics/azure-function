from datetime import datetime
import json
 
from azure.core.exceptions import (
    ResourceExistsError,
    ResourceNotFoundError
)
from azure.data.tables import TableServiceClient
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient

from env_vars_lib import get_env_vars
from extract_load_lib import (
    ETLStatus,
    ExtractBlob,
    extract_data,
    get_engine,
    get_tables_to_query,
    SalesTableComposer
)


def main(databaseconfig: dict) -> dict:

    start_time = datetime.now()
    status = ETLStatus(
        store_code=databaseconfig['nk_filial'],
        store_name=databaseconfig['nm_filial'],
        start_time=start_time
    )
    env_vars = get_env_vars()
    credential = DefaultAzureCredential()
    secret_client = SecretClient(
        vault_url=env_vars.key_vault_url, 
        credential=credential
    )
    
    try:
        conn_data = secret_client.get_secret(
            f'{databaseconfig["nk_filial"]}-LINXCONNECTION'
        )
    except ResourceNotFoundError as e:
        status.add_exception(e)
        status.status_msg = 'could not find secret data'
        status.end_time = datetime.now()
        return status.to_dict()

    conn_data = json.loads(conn_data.value)
    cod_loja = databaseconfig['nk_filial']
    tables_to_query = get_tables_to_query()
    tables_to_query.set('CONFIG', 'store_code', f'{int(cod_loja):06}')

    engine = get_engine(
        host=conn_data['host'],
        port=conn_data['port'],
        dbname=conn_data['database'],
        user=conn_data['user'],
        password=conn_data['password'],
        timeout=env_vars.sql_timeout
    )  
    dfs = {}
    for table in tables_to_query.sections()[1:]:
        try:
            query_string = tables_to_query[table]['query']
            dfs[table.lower()] = extract_data(engine=engine, query_string=query_string)
        except Exception as e:
            status.add_exception(e)
            status.status_msg = 'could not query table data'
            status.end_time = datetime.now()
            return status.to_dict()
    engine.dispose()

    extract_blob = ExtractBlob(env_vars.storage_conn_string)
    try:
        df_cadastro_cli_for = extract_blob.try_recursive_read(
            int(env_vars.registry_tables_recursive_tries),
            'parquet',
            env_vars.gold_container,
            env_vars.cadastro_cli_for_source_prefix_path,
            'cadastro_cli_for'
        )
        df_filiais = extract_blob.try_recursive_read(
            int(env_vars.registry_tables_recursive_tries),
            'parquet',
            env_vars.gold_container,
            env_vars.filiais_source_prefix_path,
            'filiais'
        )
        df_goals = extract_blob.try_recursive_read(
            int(env_vars.registry_tables_recursive_tries),
            'parquet',
            env_vars.gold_container,
            env_vars.goals_blob_path,
            env_vars.goals_blob_name
        )
        df_supervisores = extract_blob.read_blob(
            'excel',
            env_vars.bronze_container,
            env_vars.supervisores_blob_path,
            env_vars.supervisores_blob_name
        )
    except Exception as e:
        status.add_exception(e)
        status.status_msg = 'could not find batch table'
        status.end_time = datetime.now()
        return status.to_dict()
    
    try:
        sales_df = SalesTableComposer().compose(
            sales_date=datetime.today().date(),
            df_loja_venda_pgto=dfs['loja_venda_pgto'],
            df_loja_venda_pgto_lastyear=dfs['loja_venda_pgto_lastyear'],
            df_loja_venda=dfs['loja_venda'],
            df_cadastro_cli_for=df_cadastro_cli_for,
            df_filiais=df_filiais,
            df_goals=df_goals,
            df_supervisores=df_supervisores
        )
        if sales_df.empty:
            status.status_msg = 'empty sales data'
            status.end_time = datetime.now()
            return status.to_dict()
    except Exception as e:
        status.add_exception(e)
        status.status_msg = 'could not compose sales table'
        status.end_time = datetime.now()
        return status.to_dict()

    sales_df['PartitionKey'] = f'{int(cod_loja):06}'
    sales_df['RowKey'] = sales_df['FILIAL'].map(str) \
        + sales_df['TIPO_FILIAL'].map(str) \
        + sales_df['CANAL_FILIAL'].map(str) \
        + sales_df['REGIAO'].map(str) \
        + sales_df['OPERACAO_VENDA'].map(str) \
        + sales_df['CODIGO_DESCONTO'].map(str) \
        + sales_df['SUPERVISOR'].map(str) \
        + sales_df['SUPERVISOR_REGIONAL'].map(str)
    sales_dict = sales_df.to_dict(orient='records')
    transactions = [('upsert', record) for record in sales_dict]
    table_service_client = TableServiceClient.from_connection_string(
        env_vars.storage_conn_string
    )
    try:
        table_client = table_service_client.create_table(
            env_vars.realtimesales_storage_table
        )
    except ResourceExistsError:
        table_client = table_service_client.get_table_client(
            env_vars.realtimesales_storage_table
        )
        
    try:
        table_client.submit_transaction(transactions)
    except Exception as e:
        status.add_exception(e)
        status.status_msg = 'could not upload data to table storage'
        status.end_time = datetime.now()
        return status.to_dict()

    # for table_name, df in dfs.items():
    #     blob_path = 'linx_pos/RealTime/' \
    #         f'{int(cod_loja):06}_{desc_loja.replace(" ", "_")}' \
    #         f'/{table_name}/today/'
    #     today = datetime.today().date()
    #     blob_name = f'{table_name}_{today.strftime("%Y%m%d")}.parquet'
        
    #     try:
    #         load_to_blob_storage(
    #             dataframe=df,
    #             format='parquet',
    #             conn=env_vars.storage_conn_string,
    #             container=env_vars.source_container,
    #             blob_path=blob_path,
    #             blob_name=blob_name
    #         )
    #     except Exception as e:
    #         return {
    #             'store': desc_loja,
    #             'status': 'could not upload raw data to blob storage',
    #             'exception': f'{e}'
    #         }

    status.status_msg = 'loaded successfully'
    status.end_time = datetime.now()
    return status.to_dict()