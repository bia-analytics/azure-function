import logging
from env_vars_lib import get_env_vars
from extract_load_lib import ExtractBlob

 
def main(filedata: dict) -> list:
    
    env_vars = get_env_vars()

    extract_blob = ExtractBlob(filedata['conn_string'])
    df_db_list = extract_blob.try_recursive_read(
        days_to_try=int(env_vars.registry_tables_recursive_tries),
        format='parquet',
        container=filedata['container'],
        prefix_path=env_vars.database_list_prefix_path,
        prefix_name='d_filial'
    )
    df_db_list = df_db_list[df_db_list['in_real_time'] == True]

    return df_db_list.to_dict(orient='records')