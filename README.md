# Real Time Sales Azure Function

This is a collection of azure functions to compose a pipeline for streaming sales data in real time.
<br><br>

## **1 - Functions and Packages Structure**

Not all functions are connected through triggers or orchestration processes. The structure of the whole application is divided into three independent parts, as follows:

- Extract and Transform Group
    - `starter`
    - `orchestrator`
    - `get_databases`
    - `retrieve_data`
    - `upload_status`
- Load
    - `starter_upload_merged`
- Clean
    - `starter_clean`

Each of these parts works totally independent, all triggered by their own cron triggers. Moreover, the application makes use of two owned packages:

- `env_vars_lib`
- `extract_load_lib`

Finally, one important file is the `config_tables.ini` file.

Some resumed and important info about these parts are listed below.
<br><br>

### **1.1 - Extract and Transform Group**

This collection of functions is the main core of the pipleline. As the name says, it is where all the hard work of collecting and transforming data happens. Let's get into a brief explanation about each of these snippets of code:

- `starter` - starts the extract and transform process through a cron trigger. The cron string can be seen in the `function.json` file. By default it starts the function `orchestrator` every time it's triggered.
- `orchestrator` - holds the entire process through async calls, orchestrating and maintaining the state until the process ends.
- `get_databases` - a simple function to catch and return a list of databases to query.
- `retrieve_data` - below is a list of the features of this function:
    - retrieves data from "onprem" databases (incremental data) and from `Blob Storage Datalake` (registry data).
    - composes the final flat table applying all the transformations, agreggations and summarizations.
    - upserts the flat table to a `Table Storage`.
    - returns a `dict` containing info about the status of the process.
- `upload_status` - uploads to the `Blob Storage` a `json` file containing the process status of each store.
<br><br>

### **1.2 - Load**
This unique function is responsible for catching the content of the `Storage Table`, transforming it into a flat `csv` file and uploading it to a prepared folder where a `Stream Analytics` instance will query the data and compose some dashboard meaningful information.
<br><br>

### **1.3 - Clean**
This function runs at the end of the day and is responsible for cleaning all unnecessary data created during the day. The cleanning process includes:
- copying the content from the "today" table to "yesterday" table, for troubleshooting purposes
- deleting content from the "today" table.
- deleting content from the folder containing the flat table sales data.
<br><br>

### **1.4 - `env_vars_lib`**
Simple package for catching enviroment variables. All set of environment variables can be retrieved just by calling `get_env_vars`, anywhere through the entire application.
<br><br>

### **1.5 - `extract_load_lib`**
A collection of classes and functions to handle extract, transform and load processes. Additionally it handles status uploads to `Blob Storage`.
<br><br>

### **1.6 - `config_tables.ini`**
This configuration file contains data about the queries that will be executed in the stores databases.
<br><br>

## **2 - Environment Variables**
One crucial part of the application is the configuration settings set through environment variables. Azure Functions has already a set of env. variables that, in general, we should avoid changing. The set of env. variables that can be changed according to our necessities are listed below:

- `AzureWebJobsStorage` - connection string of the storage where standard data about executions, triggers and states are stored. Avoid pointing the variable to the datalake
- `STORAGE_CONN_STRING` - connection string for the storage where all the real time sales data (flat `csv`), sales table and status data will be stored.
- `TaskHub` - configuration variable that names the hub where the orchestration tasks will be stored. For more information, click [this link](https://docs.microsoft.com/en-us/azure/azure-functions/durable/durable-functions-task-hubs?tabs=csharp).
- `WEBSITE_TIME_ZONE` - standard Azure Function variable used to change in what time zone the triggers will work.
- `SQL_TIMEOUT` - time, in seconds, the script will wait for a response until it returns a timeout error.
- `BRONZE_CONTAINER` - name of the container to store and retrieve raw data.
- `SILVER_CONTAINER` - name of the container to store and retrieve normalized data.
- `GOLD_CONTAINER` - name of the container to store reliable sales data.
- `REGISTRY_TABLES_RECURSIVE_TRIES` - the application does some attempts to fetch data from the registry tables (`filiais`, for example). This variable tells the application how many recursive tries it should do.
- `CADASTRO_CLI_FOR_SOURCE_PREFIX_PATH` - the prefix of the path for the script to fetch the most recent file from the `cadastro_cli_for` table.
- `FILIAIS_SOURCE_PREFIX_PATH` - the prefix of the path for the script to fetch the most recent file from the `filiais` table.
- `STREAM_FOLDER` - the folder where flat `csv` files will be stored. `Stream Analytics` must be reading this folder.
- `REALTIMESALES_STORAGE_TABLE` - name of the `Storage Table` where the app will do the upserts. The table doesn't need to be created previously.
- `YESTERDAY_REALTIMESALES_STORAGE_TABLE` - name of the `Storage Table` where the app will store data from yesterday's sales.
- `GOALS_BLOB_PATH` - the path of the file that contains goals info.
- `GOALS_BLOB_NAME` - the name of the file that contains goals info.
- `DATABASE_LIST_PREFIX_PATH` - the prefix path of the file that contains database access info.
- `SUPERVISORES_BLOB_PATH` - the path of the file that contains data about stores governance.
- `SUPERVISORES_BLOB_NAME` - the name of the file that contains data about stores governance.
- `AZURE_CLIENT_ID` - ID used by the Azure Key Vault python library to access the resource. For more info about how this variable interacts with the app, [click here](https://docs.microsoft.com/en-us/python/api/overview/azure/keyvault-secrets-readme?view=azure-python).
- `AZURE_CLIENT_SECRET` - SECRET used by the Azure Key Vault python library to access the resource. For more info about how this variable interacts with the app, [click here](https://docs.microsoft.com/en-us/python/api/overview/azure/keyvault-secrets-readme?view=azure-python).
- `AZURE_TENANT_ID` - TENANT ID used by the Azure Key Vault python library to access the resource. For more info about how this variable interacts with the app, [click here](https://docs.microsoft.com/en-us/python/api/overview/azure/keyvault-secrets-readme?view=azure-python).
- `KEY_VAULT_URL` - URL used to initialize a Key Vault Client


<br><br>

## **3 - Tests with PyTest**
Tests were designed to work with the `pytest` package. If it's needed to develop any other tests, feel free to add them to the `tests` folder. Most of the local environment variables used by the tests are listed in the `conftest.py` file, but it lists only the variables used by more than one test file. Local environment variables for exclusive tests are listed in the related files. Below, a list of the test files and their related local environment variables:

- `test_blob_tasks.py`
    - `STORAGE_CONN_STRING`
    - `SOURCE_CONTAINER`
- `test_extract.py`
    - `STORAGE_CONN_STRING`
    - `SOURCE_CONTAINER`
    - `DB_HOST`
    - `DB_PORT`
    - `DB_NAME`
    - `DB_LOGIN`
    - `DB_PASSWORD`
    - `DB_TEST_QUERY_STRING`
    - `EXTRACT_BLOB_PREFIX_PATH`
    - `EXTRACT_BLOB_PREFIX_NAME`
    - `EXTRACT_CSV_BLOB_PATH`
    - `EXTRACT_CSV_BLOB_NAME`
    - `EXTRACT_EXCEL_BLOB_PATH`
    - `EXTRACT_EXCEL_BLOB_NAME`
- `test_get_database.py`
    - `STORAGE_CONN_STRING`
    - `SOURCE_CONTAINER`
    - `DATABASE_LIST_FILE_PATH`
    - `DATABASE_LIST_FILE_NAME`
- `test_load.py`
    - `STORAGE_CONN_STRING`
    - `SOURCE_CONTAINER`

Also, for some tests are needed dummy data, stored in the folder `dummy_data` in the `tests` folder. Some of the "dummy data" actually are not a dummy. They need to be real for the sake of the test. For this reason, these data are not present in this repo and should be locally configured in order to execute the tests successfully. The only test file that uses real data is the `test_transform_sales.py` file. Read carefully the test in order to understand what data is needed to execute the test.